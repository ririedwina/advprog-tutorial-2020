package id.ac.ui.cs.advprog.tutorial2.command.core.spell;
import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    ArrayList<Spell> spells;

    public ChainSpell(ArrayList<Spell> sepel){
        this.spells =  sepel;
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
    @Override
    public void cast() {
        for(int i =0;i<spells.size();i++){
            spells.get(i).cast();
        }
    }

    @Override
    public void undo() {
        for(int i = spells.size()-1;i>=0;i--){
            spells.get(i).undo();
        }
    }

}
