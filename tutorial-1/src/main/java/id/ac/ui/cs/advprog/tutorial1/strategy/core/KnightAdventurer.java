package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
        //ToDo: Complete me
        @Override
        public String getAlias() {
            return "Knight";
        }

    public KnightAdventurer(){
            this.setAttackBehavior(new AttackWithSword());
            this.setDefenseBehavior(new DefendWithArmor());
        }
}
