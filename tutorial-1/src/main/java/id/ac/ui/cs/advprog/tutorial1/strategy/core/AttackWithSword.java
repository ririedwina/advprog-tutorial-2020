package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
        //ToDo: Complete me
    public AttackWithSword(){
        }

    @Override
    public String getType() {
        return "sword";
    }

    @Override
    public String attack() {
        return "crash";
    }
}
