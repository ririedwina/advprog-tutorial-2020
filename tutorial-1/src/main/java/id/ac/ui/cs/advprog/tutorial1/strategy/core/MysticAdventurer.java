package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {
        //ToDo: Complete me

    @Override
    public String getAlias() {
        return "Mystic";
    }

    public MysticAdventurer(){
            this.setAttackBehavior(new AttackWithMagic());
            this.setDefenseBehavior(new DefendWithShield());
        }
}
