package id.ac.ui.cs.advprog.tutorial3.composite.core;

import javax.xml.transform.sax.SAXResult;
import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {
    private String name;
    private String role;

    public OrdinaryMember(String name, String role){
        this.name = name;
        this.role = role;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getRole() {
        return role;
    }

    @Override
    public void addChildMember(Member member) {
        return;
    }

    @Override
    public void removeChildMember(Member member) {
        return;
    }

    @Override
    public List<Member> getChildMembers() {
        return new ArrayList<>();
    }
    //TODO: Complete me
}
