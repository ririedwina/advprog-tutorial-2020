package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Shield extends Weapon {
        //TODO: Complete me
        public Shield() {
            this.weaponName = "Shield";
            this.weaponDescription = "This is shield";
            this.weaponValue = 10;
        }
}
