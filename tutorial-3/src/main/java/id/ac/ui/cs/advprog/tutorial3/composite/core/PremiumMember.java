package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    //TODO: Complete me
    private String name;
    private String role;
    private List<Member> childMemberList;

    public PremiumMember(String name, String role){
        this.name = name;
        this.role = role;
        childMemberList = new ArrayList<>();
    }
    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getRole() {
        return role;
    }

    @Override
    public void addChildMember(Member member) {
        if(role.equals("Master")) {
            childMemberList.add(member);
        }
        else {
            if(childMemberList.size() == 3) {
                return;
                }
            else{
            childMemberList.add(member);
            }
        }

    }

    @Override
    public void removeChildMember(Member member) {
        childMemberList.remove(member);
    }

    @Override
    public List<Member> getChildMembers() {
        return childMemberList;
    }
}
