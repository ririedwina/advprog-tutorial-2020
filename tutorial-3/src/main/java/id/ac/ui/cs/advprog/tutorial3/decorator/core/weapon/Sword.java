package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Sword extends Weapon {
        //TODO: Complete me
        public Sword() {
            this.weaponName = "Sword";
            this.weaponDescription = "This is sword";
            this.weaponValue = 25;
        }
}
