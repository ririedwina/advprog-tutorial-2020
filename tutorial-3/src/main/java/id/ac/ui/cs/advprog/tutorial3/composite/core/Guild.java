package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.*;

public class Guild {
    private Member guildMaster;
    private List<Member> memberList;

    public Guild(Member master) {
        this.guildMaster = master;
        this.memberList = new ArrayList<>();
    }

    public void addMember(Member parent, Member child) {
        //TODO: Complete me
        parent.addChildMember(child);
    }

    public void removeMember(Member parent, Member child) {
        //TODO: Complete me
        parent.removeChildMember(child);
        memberList.remove(child);
    }

    public List<Member> getMemberHierarchy() {
        return new ArrayList<>(Arrays.asList(guildMaster));
    }

    public List<Member> getMemberList() {
        //TODO: Complete me
        memberList = new ArrayList<>();
        getMemberListHelper(guildMaster);
        return memberList;

    }

    public Member getMember(String name, String role) {
        return getMemberHelper(this.guildMaster, name, role);
    }

    private void getMemberListHelper(Member member) {
        memberList.add(member);
        for (Member m: member.getChildMembers()) {
            this.getMemberListHelper(m);
        }
    }

    private Member getMemberHelper(Member member, String name, String role) {
        if (member.getName().equals(name) && member.getRole().equals(role)) {
            return member;
        }

        if (member instanceof OrdinaryMember) {
            return null;
        }

        for (Member m: member.getChildMembers()) {
            Member find = this.getMemberHelper(m, name, role);
            if (find != null) {
                return find;
            }
        }

        return null;
    }
}
