package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class UniqueUpgrade extends Weapon {

    Weapon weapon;

    public UniqueUpgrade(Weapon weapon){

        this.weapon = weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }


    // Senjata bisa dienhance hingga 10-15 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        Random r = new Random();
        if (weapon == null) return r.nextInt(6) + 10;
        return r.nextInt(6) + 10 + weapon.getWeaponValue();
    }


    @Override
    public String getDescription() {
        //TODO: Complete me
        return weapon.getDescription() + " but better";
    }
}
