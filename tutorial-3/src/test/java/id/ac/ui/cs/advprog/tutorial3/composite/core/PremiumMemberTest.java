package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("Gold Merchant", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member baru = new OrdinaryMember("Aksara", "Ordinary");
        member.addChildMember(baru);
        assertEquals(1, member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member baru = new PremiumMember("Aksara", "Premium");
        member.addChildMember(baru);
        member.removeChildMember(baru);
        assertEquals(0, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member baru = new PremiumMember("Aksara", "Premium");
        Member barulagi = new PremiumMember("Dunya", "Premium");
        Member barulagilagi = new PremiumMember("Arjuna", "Premium");
        Member terakhir = new PremiumMember("Edwina", "Premium");
        member.addChildMember(baru);
        member.addChildMember(barulagi);
        member.addChildMember(barulagilagi);
        member.addChildMember(terakhir);
        assertEquals(3, member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member guildMaster = new PremiumMember("Eko", "Master");
        Guild guild = new Guild(guildMaster);

        Member baru = new PremiumMember("Aksara", "Premium");
        Member barulagi = new PremiumMember("Dunya", "Premium");
        Member barulagilagi = new PremiumMember("Arjuna", "Premium");
        Member terakhir = new PremiumMember("Edwina", "Premium");

        guildMaster.addChildMember(baru);
        guildMaster.addChildMember(barulagi);
        guildMaster.addChildMember(barulagilagi);
        guildMaster.addChildMember(terakhir);
        assertEquals(4, guildMaster.getChildMembers().size());
    }
}
