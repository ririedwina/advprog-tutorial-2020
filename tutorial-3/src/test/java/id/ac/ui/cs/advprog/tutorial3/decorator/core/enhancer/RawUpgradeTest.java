package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Longbow;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Shield;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RawUpgradeTest {

    private RawUpgrade rawUpgrade;

    @BeforeEach
    public void setUp(){
        rawUpgrade = new RawUpgrade(new Shield());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        assertEquals("Shield", rawUpgrade.getName());
    }

    @Test
    public void testMethodGetDescription(){
        //TODO: Complete me
        assertEquals("This is shield but better", rawUpgrade.getDescription());
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        assertTrue(rawUpgrade.getWeaponValue() >= 15 && rawUpgrade.getWeaponValue() <= 20);

    }
}
