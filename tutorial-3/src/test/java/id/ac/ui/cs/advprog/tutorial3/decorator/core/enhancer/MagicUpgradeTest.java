package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Longbow;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MagicUpgradeTest {


    private MagicUpgrade magicUpgrade;

    @BeforeEach
    public void setUp(){
        magicUpgrade = new MagicUpgrade(new Longbow());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        assertEquals("Longbow", magicUpgrade.getName());
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        assertEquals("This is longbow but better", magicUpgrade.getDescription());
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        assertTrue(magicUpgrade.getWeaponValue() >= 30 && magicUpgrade.getWeaponValue() <= 35);
    }
}
