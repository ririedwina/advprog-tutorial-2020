package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class WeaponProducerTest {

    Weapon weapon1;
    Weapon weapon2;
    Weapon weapon3;
    Weapon weapon4;

    @Test
    public void testCreateWeaponEnhancer() {
        weapon1 = WeaponProducer.WEAPON_GUN.createWeaponEnhancer();
        weapon2 = WeaponProducer.WEAPON_LONGBOW.createWeaponEnhancer();
        weapon3 = WeaponProducer.WEAPON_SHIELD.createWeaponEnhancer();
        weapon4 = WeaponProducer.WEAPON_SWORD.createWeaponEnhancer();
        //TODO: Complete me
        assertEquals("Gun", weapon1.getName());
        assertEquals("Longbow", weapon2.getName());
        assertEquals("Shield", weapon3.getName());
        assertEquals("Sword", weapon4.getName());
    }
}
