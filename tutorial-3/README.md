[![pipeline status](https://gitlab.com/ririedwina/advprog-tutorial-2020/badges/tutorial-3/pipeline.svg)](https://gitlab.com/ririedwina/advprog-tutorial-2020/-/commits/tutorial-3)
[![coverage report](https://gitlab.com/ririedwina/advprog-tutorial-2020/badges/tutorial-3/coverage.svg)](https://gitlab.com/ririedwina/advprog-tutorial-2020/-/commits/tutorial-3)
# Tutorial 3

# I Want to Recruit Members for My Guild and Enhance My Weapons

Setelah pada tutorial sebelumnya (tutorial-2) Anda telah menjalankan amanah untuk membantu para adventurer melakukan 
***contract seal*** dengan ***spirit*** melalui perantara ***spell*** dan membantu para ***spirit*** melakukan ***skill spesial***, 
kali ini di `tutorial-3` kita akan mencoba menggunakan sihir Spring untuk membangun sebuah ***guild*** dari 
berbagai macam ***member*** dan ***meng-upgrade weapon*** dari para member.

## Broken Memory

Guild master duduk tersenyum riang di hadapan anda. Dengan segelas kopi ditangan kanannya, ia mengajak anda melihat sebuah dokumen.

"Anda sudah banyak membantu kami di sini. Kemampuan untuk membantu spirit sungguh membuat saya terkesan. Kemampuan anda
Seolah anda berasal dari dunia lain. "

Guild master tertawa sembari menepuk lirih pundak anda. Tepukan itu hanya sebuah sebuah bentuk candaan bagi Guild Master,
tapi entah kenapa bagi anda, tepukan itu begitu terasa telak. Wajah anda sedikit pucat, takut bahwa suatu kenyataan yang anda sembunyikan selama
ini, muncul ke permukaan. Suatu hal yang [] minta anda rahasiakan. Pikiran anda melayang beberapa saat. Anda penasaran dimana [] berada saat ini?
Kenapa dia memilih anda untuk datang ke dunia ini? Lalu, kenapa dia seperti menawan ingatan anda ketika berada di dunia ini. Sosoknya begitu
mencurigakan semakin anda memikirkan sosoknya.  

Kuping anda berdenging, membawa kesadaran anda semakin jauh. Beberapa informasi asing masuk ke kepala anda. 

*Design pattern lagi ya.. Tapi sepertinya akan berguna*

" Apa kau mendengarkan? "

Suara Guild Master membawa kesadaran anda kembali. 

" Maaf, mari kita lanjutkan."


" Apa anda tidak apa-apa? Anda kelihatan kurang sehat. Wajah anda juga pucat."

"Ah bukan masalah. Aku hanya memikirkan beberapa hal. Mari kita lanjutkan." 

## (Composite Pattern)
------------------------

Dengan menggunakan sihir Spring, Anda akan melakukan open recruitment member untuk ***guild***. 
Namun, ***Guild*** ini menerapkan sistem seperti MLM (Multi Level Marketing) yang tiap membernya memiliki atasan
ataupun bawahan.
 
Terdapat dua jenis member dalam ***Guild***, yaitu ***Ordinary Member*** dan ***Premium Member***.

1. ***Ordinary Member*** merupakan bawahan dari ***Premium Member*** atau dengan kata lain
berada pada tingkat bawah dan tidak dapat memiliki member bawahan.

2. ***Premium Member*** merupakan member yang dapat memiliki bawahan maksimal sebanyak tiga orang, baik
***Premium Member*** maupun ***Ordinary Member***.

Di dalam ***Guild*** terdapat seorang member khusus yaitu ***Guild Master***. ***Guild Master*** memiliki role khusus
yaitu **Master**, yang mana role **Master** ini hanya ada satu di dalam sebuah ***Guild***.

Perbedaan lain antara ***Guild Master*** dengan ***Premium Member***:
- ***Guild Master*** merupakan seorang ***Premium Member*** yang tidak memiliki atasan di dalam ***Guild***.
- ***Guild Master*** dapat menambahkan bawahan sebanyak apapun, tidak seperti ***Premium Member*** yang memiliki jumlah
maksimal.
   
Seluruh pengaturan member berada pada ***Guild***. ***Guild*** akan mengatur hierarki para member. Tugas kalian
di sini adalah mengatur konfigurasi pada ***Guild*** dan dua tipe tingkatan member, yaitu ***OrdinaryMember*** dan
***PremiumMember*** agar memenuhi ketentuan di atas.

### TODO List Composite Pattern
- [ ] Mengimplementasikan semua implementasi yang dibutuhkan pada `OrdinaryMember.java`.
- [ ] Mengimplementasikan semua implementasi yang dibutuhkan pada `PremiumMember.java`.
- [ ] Mengimplementasikan `addMember`, `removeMember`, dan `getMemberList` pada `Guild.java`.

## (Decorator Pattern)
------------------------
Setelah membangun sebuah ***Guild*** yang memiliki hirarki ***member***, kalian akan membantu para ***member*** dalam
meng-upgrade ***weapon*** mereka. 

Terdapat 4 macam weapon, yaitu:

1. ***Gun*** dengan tipe Automatic Gun yang memiliki default value 20.
2. ***Longbow*** dengan tipe Big Longbow yang memiliki default value 15.
3. ***Shield*** dengan tipe Heater Shield yang memiliki default value 10.
4. ***Sword*** dengan tipe Great Sword yang memiliki default value 25.

Keempat macam ***weapon*** tersebut dapat di-upgrade dengan berbagai macam ***enhancer***.

Berikut efek yang didapatkan oleh suatu ***weapon*** setelah diberikan suatu ***enhancer***.

1. ***Regular Upgrade*** dapat menambah value dari suatu weapon sebanyak kira-kira ***1-5*** poin.
2. ***Raw Upgrade*** dapat menambah value dari suatu weapon sebanyak kira-kira ***5-10*** poin.
3. ***Unique Upgrade*** dapat menambah value dari suatu weapon sebanyak kira-kira ***10-15*** poin.
4. ***Magic Upgrade*** dapat menambah value dari suatu weapon sebanyak kira-kira ***15-20*** poin.
5. ***Chaos Upgrade*** dapat menambah value dari suatu weapon sebanyak kira-kira ***50-55*** poin.

Lalu, berikut upgrade yang dapat dilakukan oleh masing-masing ***weapon***.
1. ***Gun*** dapat melakukan ***Raw Upgrade*** dan ***Regular Upgrade***.
2. ***Longbow*** dapat melakukan ***Regular Upgrade***, ***Magic Upgrade***, dan ***Unique Upgrade***.
3. ***Shield*** dapat melakukan ***Raw Upgrade*** dan ***Magic Upgrade***.
4. ***Sword*** dapat melakukan ***Unique Upgrade*** dan ***Magic Upgrade***.

### TODO List Decorator Patterm
- [ ] Mengimplementasikan semua implementasi yang dibutuhkan pada `Gun.java`.
- [ ] Mengimplementasikan semua implementasi yang dibutuhkan pada `Longbow.java`.
- [ ] Mengimplementasikan semua implementasi yang dibutuhkan pada `Shield.java`.
- [ ] Mengimplementasikan semua implementasi yang dibutuhkan pada `Sword.java`.
- [ ] Mengimplementasikan `getWeaponValue` dan `getDescription` pada `Regular.java`.
- [ ] Mengimplementasikan `getWeaponValue` dan `getDescription` pada `Raw.java`.
- [ ] Mengimplementasikan `getWeaponValue` dan `getDescription` pada `Unique.java`.
- [ ] Mengimplementasikan `getWeaponValue` dan `getDescription` pada `Magic.java`.
- [ ] Mengimplementasikan `getWeaponValue` dan `getDescription` pada `Chaos.java`.
- [ ] Mengimplementasikan `enhanceToAllWeapons` pada `EnhanceRepository.java`.

## (Test untuk `core` Pada Tiap Pattern)
------------------------
Lengkapilah seluruh method test untuk `core` di tiap pattern. Lakukan perubahan pada badge pipelien readme sesuai dengan pengaturan repo anda.

***Hint: Gunakan AssertEquals atau AssertTrue*** 

## Notes
Tugas kalian pada 2 pattern di atas adalah melengkapi tiap implementasi yang ditandai oleh `//TODO: Complete me`
dan membuat test untuk `core` pada masing-masing pattern.
